#! /bin/bash

while read p;
do
	if [[ "$p" == "######"* ]];
	then
		F_CPU="";
		MCU="";
		BOARD="";
		VARIANT="";
		VID="";
		PID="";

		continue;
	fi;
	
	_F_CPU=$(echo "$p" | sed -n 's/.*\.build\.f_cpu=\(\d*\)/\1/p');
	_MCU=$(echo "$p" | sed -n 's/.*\.build\.mcu=\(\d*\)/\1/p');
	_BOARD=$(echo "$p" | sed -n 's/.*\.build\.board=\(.*\)/\1/p');
	_VARIANT=$(echo "$p" | sed -n 's/.*\.build\.variant=\(\d*\)/\1/p');
	_VID=$(echo "$p" | sed -n 's/.*\.build\.vid=\(.*\)/\1/p');
	_PID=$(echo "$p" | sed -n 's/.*\.build\.pid=\(.*\)/\1/p');
	
	[[ ! -z "$_F_CPU" ]] && F_CPU="$_F_CPU";
	[[ ! -z "$_MCU" ]] && MCU="$_MCU";
	[[ ! -z "$_BOARD" ]] && BOARD="$_BOARD";
	[[ ! -z "$_VARIANT" ]] && VARIANT="$_VARIANT";
	[[ ! -z "$_VID" ]] && VID="$_VID";
	[[ ! -z "$_PID" ]] && PID="$_PID";
	
	echo "$BOARD -- $MCU -- $F_CPU";
	
	make clean;
	[[ ! -f "$BOARD_$MCU_F_CPU.a" ]] &&
		[[ ! -z "$VARIANT" ]] && [[ ! -z "$F_CPU" ]] && [[ ! -z "$MCU" ]] && [[ ! -z "$BOARD" ]] &&
				make -j6 all VARIANT="$VARIANT" F_CPU="$F_CPU" MCU="$MCU" BOARD="$BOARD" VID="$VID" PID="$PID";
	
done <<<$(grep "build\.mcu\|build\.f_cpu\|build\.board\|build\.vid\|build\.pid\|build\.variant\|######" ../boards.txt);

make clean;
